---
short-description: Tutorials and Manuals
render-subpages: false
...

# Documentation and Tutorials

Feel free to jump straight to the download section, start practicing
with the tutorials, or read the F.A.Q. if you don’t know what this is
all about.

## General

 * [Application Development Manual (Read this first)](application-development/index.md)
 * [Frequently Asked Questions](frequently-asked-questions/index.md)
 * [Plugin Writer's Guide](plugin-development/index.md)
 * <a href="/data/doc/gstreamer/head/gstreamer/html/">Core Reference</a>
 * <a href="/data/doc/gstreamer/head/gstreamer-libs/html/">Core Libraries Reference</a>
 * [Core Design Documentation](design/index.md)
 * [GStreamer 0.10 to 1.0 porting guide](https://gitlab.freedesktop.org/gstreamer/gstreamer/raw/master/docs/random/porting-to-1.0.txt)

<!-- FIXME: save useful bits from wiki
| GStreamer Wiki (see esp. <a href="&site;/wiki/ReleasePlanning">ReleasePlanning</a> and <a href="&site;/wiki/SubmittingPatches">SubmittingPatches</a>)
| <a href="&site;/wiki/">HTML</a> |
-->


## GStreamer APIs References

* [GStreamer Core library](gstreamer)
* [GStreamer Libraries Reference](libs.md)
* [GStreamer Plugins Reference](gst-index)

> ![Warning](images/icons/emoticons/warning.svg) Only the API in libraries from
> GStreamer core and gst-plugins-base are guaranteed to be API and ABI stable


## Other modules

 * [GStreamer Editing Services Reference](gst-editing-services)
 * [GStreamer RTSP Server Reference](gst-rtsp-server)
 * [GStreamer VAAPI Reference](vaapi)
 * [GStreamer Validate](gst-devtools)
 * <a href="/data/doc/orc/">Orc - Optimized inner loop Runtime Compiler</a>

## GStreamer Conference Videos and Slides

* [GStreamer Conference 2016: Videos and Slides] [(PDF slides)]
* [GStreamer Conference 2015: Videos and Slides] [(PDF slides)][1]
* [GStreamer Conference 2014: Videos and Slides] [(PDF slides)][2]
* [GStreamer Conference 2013: Videos and Slides] [(PDF slides)][3]
* [GStreamer Conference 2012: Videos and Slides] [(PDF slides)][4]
* [GStreamer Conference 2011: Videos and Slides] [(PDF slides)][5]
* [GStreamer Conference 2010: Videos and Slides] [(PDF slides)][6]

  [GStreamer Conference 2016: Videos and Slides]: http://gstconf.ubicast.tv/channels/#gstreamer-conference-2016
  [GStreamer Conference 2015: Videos and Slides]: http://gstconf.ubicast.tv/channels/#gstreamer-conference-2015
  [GStreamer Conference 2014: Videos and Slides]: http://gstconf.ubicast.tv/channels/#gstreamer-conference-2014
  [GStreamer Conference 2013: Videos and Slides]: http://gstconf.ubicast.tv/channels/#gstreamer-conference-2013
  [GStreamer Conference 2012: Videos and Slides]: http://gstconf.ubicast.tv/channels/#gstreamer-conference-2012
  [GStreamer Conference 2011: Videos and Slides]: http://gstconf.ubicast.tv/channels/#conferences-2011
  [GStreamer Conference 2010: Videos and Slides]: http://gstconf.ubicast.tv/channels/#conferences-2010
  [(PDF slides)]: https://gstreamer.freedesktop.org/data/events/gstreamer-conference/2016/
  [1]: https://gstreamer.freedesktop.org/data/events/gstreamer-conference/2015/
  [2]: https://gstreamer.freedesktop.org/data/events/gstreamer-conference/2014/
  [3]: https://gstreamer.freedesktop.org/data/events/gstreamer-conference/2013/
  [4]: https://gstreamer.freedesktop.org/data/events/gstreamer-conference/2012/
  [5]: https://gstreamer.freedesktop.org/data/events/gstreamer-conference/2011/
  [6]: https://gstreamer.freedesktop.org/data/events/gstreamer-conference/2010/


## GStreamer licensing advisory

The GStreamer community provides the following licensing advisory for
developers planing on or already using GStreamer for their applications:

 * [GStreamer Licensing Advisory](licensing.md)


## GStreamer plugin module split-up

[An explanation of the various plugin modules and how they were split up](splitup.md)

## RTP and RTSP support in GStreamer

 * [RTSP and RTP component overview](rtp.md)
